"use strict";

function id(el) {
	return document.getElementById(el);
}

function store(info) {
	localStorage.setItem(info, id(info).value);
}

function strg(info) {
	return localStorage.getItem(info);
}

console.log(localStorage);

id("submitLogin").addEventListener("click", function() {
	if (
		id("login").value === localStorage.getItem("login") &&
		id("password").value === localStorage.getItem("password")
	) {
		id("form_login").setAttribute("action", "login_page.html");
	} else {
		alert("Informations de connexion erronnées, veuillez recommencer");
	}
});
