"use strict";

function id(el) {
	return document.getElementById(el);
}

function store(info) {
	localStorage.setItem(info, id(info).value);
}

function strg(info) {
	return localStorage.getItem(info);
}

console.log(localStorage);

let profileTemplate = `<h1>Profil</h1>
<h2>Bonjour, <span style="color: ${strg("favcolor")}">
${
	strg("gender") === "male"
		? "M."
		: strg("gender") === "female"
		? "Mme."
		: "Mx."
} 
${strg("firstname")} 
${strg("lastname")} 
(@${strg("login")})
</span></h2>
<ul>
	<li>Date de naissance : ${strg("birthday")}</li>
	<li>Ville : ${strg("city")}</li>
	<li>E-mail : ${strg("email")}</li>
	<li>Téléphone : ${strg("phone")}</li>
	<li>Site web : <a href="${strg("website")}">${strg("website")}</a></li>
	<li>Hobbies : ${strg("hobbies")}</li>
</ul>`;

id("profile").innerHTML = profileTemplate;
