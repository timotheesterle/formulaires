"use strict";

function id(el) {
	return document.getElementById(el);
}

function store(info) {
	localStorage.setItem(info, id(info).value);
}

function strg(info) {
	return localStorage.getItem(info);
}

console.log(localStorage);

id("submitRegister").addEventListener("click", function() {
	if (strg("login") === id("login").value) {
		alert("Ce nom d'utilisateur est déjà pris");
	} else {
		id("form_register").setAttribute("action", "./register_success.html");
		store("login");
		store("password");
		store("lastname");
		store("firstname");
		store("gender");
		store("birthday");
		store("city");
		store("email");
		store("phone");
		store("website");
		store("favcolor");
		store("hobbies");
	}
});
